<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Project1.ru</title>
    <base target="_blank">
</head>
<body>
<p><a href="1.html">Project1.ru</a></p>

<?php
$memcache = new Memcache;
$memcache->connect('memcached', 11211) or die ("Could not connect");


$redis = new Redis();
$redis->connect('redis', 6379);
if ($redis->ping()) {
    echo "PONGn";
}

$host='postgres';
$db = 'postgres';
$username = 'postgres';
$password = 'postgres';

# Создаем соединение с базой PostgreSQL с указанными выше параметрами
$dbconn = pg_connect("host=$host port=5432 dbname=$db user=$username password=$password");

if (!$dbconn) {
    die('Could not connect');
}
else {
    echo("Connected to local DB");
}
# Сделаем запрос на получение строк с id=2
$res = pg_query($dbconn, "select * from users");

# Выведем полученные строки
while ($row = pg_fetch_row($res)) {
echo "<br />\n";
echo "Id: $row[0], Name: $row[1], Phone: $row[2]";
echo "<br />\n";


$redis->set($row[0], $row[1].' '.$row[2]);
$memcache->set($row[0], $row[1].' '.$row[2],false, $row[3] );


}


?>
</body>
</html>
